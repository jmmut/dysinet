#!/bin/bash

#to run just 1 test:
# cd build;make;ok=$?;cd ..;if [ 0 -eq $ok ] ; then ./build/bin/test_async; fi

set -uo pipefail

cd build
conan build ..
cd ..

function execute_tests () {
	local FAILS=0
	while read file
	do
		echo ""
		echo "    ---    executing $file"
		$file 2>&1 | grep "^TestSuite"
		CODE=$?
		#echo "exit code: $CODE"
		FAILS=$(($FAILS + $CODE))
		#echo "fails: $FAILS"
	done

	echo ""
	#echo $FAILS
	if [ $FAILS -ne 0 ]
	then
		echo "Some tests failed!"
	else
		echo "All tests passed"
	fi
}

ls ./build/bin/test* | execute_tests


