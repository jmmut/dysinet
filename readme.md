# Dysinet (Dynamically Sized Network)

The idea of this project is to have a system similar to a neural network but that can detect its own saturation and grow accordingly.

See section "Rationale" below for more details.

## Requirements to compile

- C++ compiler (`sudo apt-get install build-essential`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)
- conan remotes for jmmut and bincrafters (`conan remote add jmmut_remote https://api.bintray.com/conan/jmmut/commonlibs` and `conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan`)


## Get dependencies, compile and run

Linux:
```
git clone https://bitbucket.org/jmmut/dysinet.git
cd dysinet
mkdir build && cd build
conan install .. --build missing
conan build ..
./bin/dysinet
```

### Tests

To find the resources, the tests must be run from the project root folder.
```
gunzip -k resources/*
./runTests.sh
```

## Rationale

One of the biggest differences between real and artificial neural networks is that the tweaking in artificial ones consists in changing the weights of the connections, and in the real ones consists mostly in creating new connections.

One side effect of this is that artificial networks don't cope well with long-term memories, because the capacity of a group of neurons is finite and when it saturates, old patterns are forgotten.

This project is a silly experiment about abstracting a group of neurons and detecting when it is handling too many different patterns, and creating new groups of neurons dynamically in a hierarchical way.

Another idea that would be interesting to add here is Hebbian learning: the notion that neurons that fire together, get new connections to each other. However I still don't know how to express that within the model of this project.

### Limitations

The initial design is synchronous, doesn't use sparse data, and doesn't support fuzzy patterns. These are things that probably should be present in the long term. 

### Experiment comparing AtemporalDysinet with HierarchicalDysinet:

#### Hierarchical
```
1 2 3 4
[1, 0]
information: 0
1 2 4 5
[1, 1, 0]
information: 2
1 2 5 6
[1, 1, 1, 0]
information: 3.16993
2 3 3 4
[1, 1, 1, 1, 0]
information: 4.31128
p
{i1: [([1, 2], [1, 0]), ([2, 3], [1, 1, 0])], i2: [([3, 4], [1, 0]), ([4, 5], [1, 1, 0]), ([5, 6], [1, 1, 1, 0])], main: [([1, 0, 1, 0], [1, 0]), ([1, 0, 1, 1, 0], [1, 1, 0]), ([1, 0, 1, 1, 1, 0], [1, 1, 1, 0]), ([1, 1, 0, 1, 0], [1, 1, 1, 1, 0])]}
```

#### Atemporal
```
1 2 3 4
[1, 0]
information: 0
1 2 4 5
[1, 1, 0]
information: 1
1 2 5 6
[1, 1, 1, 0]
information: 1.58496
2 3 3 4
[1, 1, 1, 1, 0]
information: 2
p
[([1, 2, 3, 4], [1, 0]), ([1, 2, 4, 5], [1, 1, 0]), ([1, 2, 5, 6], [1, 1, 1, 0]), ([2, 3, 3, 4], [1, 1, 1, 1, 0])]
```

#### Comparison of last layer
```
[([1, 0, 1, 0], [1, 0]), ([1, 0, 1, 1, 0], [1, 1, 0]), ([1, 0, 1, 1, 1, 0], [1, 1, 1, 0]), ([1, 1, 0, 1, 0], [1, 1, 1, 1, 0])]
[([1, 2, 3, 4], [1, 0]), ([1, 2, 4, 5], [1, 1, 0]), ([1, 2, 5, 6], [1, 1, 1, 0]), ([2, 3, 3, 4], [1, 1, 1, 1, 0])]
```
It seems that the Hierarchical measures more entropy than the Atemporal, so the Hierarchical is adding artificial 
entropy, even though the final mapping is the same 
(4 labels that correspond to inputs `[1 2 3 4], [1 2 4 5], [1 2 5 6], [2 3 3 4]`).

However, it's worth noting that the Hierarchical has only 2 and 3 labels in the first layer, and the final layer
has 4 labels of labels, and the Atemporal has 4 labels of inputs. 4 labels of inputs might be more costly than 4 labels of labels, as labels are supposed to be shorter, just names for the inputs.

In test_unification, it's shown how that the Hierarchical is between 1.2 and 1.9 times faster at guessing than the Atemporal on big inputs (1000 elements), even though Atemporal is faster on short inputs. They even out at ~10 elements.