/**
 * @file TestSuite.h
 * @author jmmut
 * @date 2015-10-10.
 */

#ifndef COMMONLIBS_TESTSUITE_H
#define COMMONLIBS_TESTSUITE_H

#include <vector>
#include <set>
#include <functional>
#include <iostream>
#include <string>
#include "StackTracedException.h"
#include "log.h"

namespace randomize::test {

    /**
     * to use like this:
    ```
    int main (int argc, char **argv) {
        TestSuite{"my test suite or class test", {
                test1, test2,
        });
    }
    ```
     * TODO change this description
     * You can use the name to differentiate between TestSuites.
     * the tests must have the same prototype as the main, to be able to receive arguments. they must
     * return 0 if the test passed, and other value otherwise. An easy way to think about it is
     * returning how many fails detected the test. Example:
    ```
    int test1 (int argc, char **argv) {
        int fails = 0;
        fails += (2+2 != 4);
        fails += !(somethingThatMustBeTrue())
        return fails;
    }
    ```
     */
    class TestSuite {
    private:
        std::string name;
        std::vector<std::function<void()>> functions;
        std::set<int> failedTests;

    public:
        TestSuite(std::string name, std::initializer_list<std::function<void()>>);
        unsigned long countFailed() const;

        void run();
    };

#define ASSERT_OR_THROW(expression) ASSERT_OR_THROW_MSG((expression), "")

#define ASSERT_OR_THROW_MSG(expression, message) \
    if (not (expression)) \
        throw randomize::exception::StackTracedException(\
                __FILE__ ":" + std::to_string(__LINE__) + (": assertion ("  #expression  ") failed: ")\
                        + std::to_string(expression) + ". " + (message))

#define ASSERT_EQUALS_OR_THROW(actual, expected) ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw randomize::exception::StackTracedException( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed: ") \
                + std::to_string(actual) + " == " + std::to_string(expected) \
                + ". " + (message))


#define ASSERT_STRING_EQUALS_OR_THROW(actual, expected) ASSERT_STRING_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_STRING_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw randomize::exception::StackTracedException( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed: ") \
                + std::string(actual) + " == " + std::string(expected) \
                + ". " + (message))

};

#define ASSERT_EQUALS_OR_THROW_SS(actual, expected) ASSERT_EQUALS_OR_THROW_SS_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_SS_MSG(actual, expected, message) \
    if (const auto &_actualResult{actual}; true) \
        if (const auto &_expectedResult{expected}; not ((_actualResult) == (_expectedResult))) \
            if (std::stringstream comparisonMessage; true) \
                throw randomize::exception::StackTracedException( \
                    ((comparisonMessage << __FILE__ << ":" << __LINE__ \
                        << (": assertion (" #actual " == " #expected ") failed: ") \
                        << _actualResult << " == " << _expectedResult \
                        << ". " << message), comparisonMessage.str()))


#endif //COMMONLIBS_TESTSUITE_H
