/**
 * @file Walkers.h
 * @author jmmut 
 * @date 2016-02-15.
 */

#ifndef COMMONLIBS_WALKERS_H
#define COMMONLIBS_WALKERS_H

#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include <numeric>


namespace randomize::string {

    /**
    * A container is anything that has defined `.empty()`, `.begin()` and `.end()` and can be iterated with them.
    * To print the inner types, the operator<< has to be defined for them.
    */
    template<typename C>
    std::string to_string(const C &container, std::string open, std::string separator, std::string close) {
        if (container.empty()) {
            return open + close;
        } else {
            std::stringstream ss;
            ss << open;
            auto it = container.begin();
            ss << *it;
            ++it;
            for (; it != container.end(); ++it) {
                ss << separator << *it;
            }
            ss << close;
            return ss.str();
        }
    }
    template<typename C>
    std::string container_to_string(const C &v) {
        return to_string(v, "[", ", ", "]");
    }



    template<typename C, typename F>
    std::string container_to_string(const C &v, F element_to_string) {
        if (v.empty()) {
            return "[]";
        } else {

            std::string joined = std::accumulate(
                    std::begin(v),
                    std::end(v),
                    std::string(),
                    [&element_to_string](std::string s, decltype(*std::begin(v)) elem) {
                        std::stringstream ss;
                        ss << s << " " << element_to_string(elem) << ",";
                        return ss.str();
                    }
            );
            joined[0] = '[';
            joined[joined.size() - 1] = ']';
            return joined;
        }
    }
};

#endif //COMMONLIBS_WALKERS_H
