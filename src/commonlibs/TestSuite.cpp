/**
 * @file TestSuite.cpp
 * @author jmmut
 * @date 2015-10-10.
 */

#include "TestSuite.h"

#include <utility>

namespace randomize::test {
    using std::string;

    TestSuite::TestSuite(string name, std::initializer_list<std::function<void()>> mains)
            : name(std::move(name)), functions{mains} {
        run();
    }

    void TestSuite::run() {
        int i = 0;
        for (auto &func : functions) {
            try {
                func();
            } catch (std::exception &except) {
                std::cout << "test " << name << "[" << i << "]: " << except.what() << std::endl;
                failedTests.insert(i);
            }
            i++;
        }

        std::cout << "TestSuite: " << failedTests.size() << "/" << functions.size() << " "
                  << name << " tests failed " << std::endl;

        if (not failedTests.empty()) {
            string tests;
            for (auto index : failedTests) {
                tests += std::to_string(index);
                tests += ", ";
            }
            LOG_DEBUG("failed tests indices are: %s", tests.c_str());
        }
    }

    unsigned long TestSuite::countFailed() const {
        return failedTests.size();
    }
};
