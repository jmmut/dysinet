/**
 * @file MnistLoader.h
 * @date 2021-11-06
 */

#ifndef DYSINET_MNISTLOADER_H
#define DYSINET_MNISTLOADER_H

#include <string>
#include <vector>
#include "mnist/Input.h"


struct Image {
    size_t width;
    size_t height;
    Input pixels;
};


std::vector<Image> loadImageRange(std::string filename, long firstImageIndex, long lastImageIndex);
std::vector<Byte> loadLabelRange(std::string filename, long firstLabelIndex, long lastLabelIndex);

std::string printAscii(const Image &image);



#endif //DYSINET_MNISTLOADER_H
