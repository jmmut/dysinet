/**
 * @file MnistLoader.cpp
 * @date 2021-11-06
 */

#include "MnistLoader.h"
#include <fstream>
#include <sstream>
#include "commonlibs/StackTracedException.h"

using MnistException = randomize::exception::StackTracedException;
// using MnistException = std::runtime_error; // use this to skip including randomize's commonlibs

static const int MAGIC_NUMBER_MSB_FIRST = 2049;
static const int MAGIC_NUMBER_IMAGES = 2051;

int32_t readIntBigEndian(std::ifstream &file) {
    int32_t result = 0;
    Byte c;
    for (int i = 0; i < 4; ++i) {
        file >> c;
        result = (result << 8u) + c;
    }
    return result;
}

long readHeader(std::ifstream &file, std::string filename, int expectedMagic, long firstElemIndex, long lastElemIndex) {
    if (not file) {
        throw MnistException{"file not found: " + filename};
    }
    auto magicNumber = readIntBigEndian(file);
    if (magicNumber != expectedMagic) {
        throw MnistException{"magic number " + std::to_string(magicNumber) + " doesn't match expected "
                                   + std::to_string(expectedMagic)};
    }
    auto elementCount = readIntBigEndian(file);
    if (firstElemIndex < 0 or firstElemIndex >= elementCount) {
        throw MnistException{"firstElemIndex must be between 0 and "
                                   + std::to_string(elementCount -1) + ", both included"};
    }
    if (lastElemIndex > elementCount or lastElemIndex < (-elementCount + firstElemIndex)) {
        throw MnistException{
                "with firstElemIndex=" + std::to_string(firstElemIndex) + ", lastElemIndex must be between "
                + std::to_string(-elementCount + firstElemIndex) + " and " + std::to_string(elementCount - 1)
                + ", both included"};
    }
    return elementCount;
}


std::vector<Image> loadImageRange(std::string filename, long firstImageIndex, long lastImageIndex) {
    std::ifstream file{filename};
    auto elementCount = readHeader(file, filename, MAGIC_NUMBER_IMAGES, firstImageIndex, lastImageIndex);
    auto rows = readIntBigEndian(file);
    auto columns = readIntBigEndian(file);
    char byte;
    for (int i = 0; i < firstImageIndex * rows * columns; ++i) {
        if (not file) {
            throw MnistException{"unexpected end of file!"};
        }
        file.get(byte);
    }

    auto requestedImageCount = lastImageIndex - firstImageIndex;
    requestedImageCount += lastImageIndex >= 0 ? 0 : elementCount; // using negative lastImageIndex means cyclic indexes

    std::vector<Image> images;

    for (int i = 0; i < requestedImageCount; ++i) {
        images.emplace_back();
        images.back().height = rows;
        images.back().width = columns;
        for (int j = 0; j < rows * columns; ++j) {
            if (not file) {
                throw MnistException{"unexpected end of file!"};
            }
            file.get(byte);
            images.back().pixels.push_back(static_cast<Byte>(byte));
        }
    }

    return images;
}


std::vector<Byte> loadLabelRange(std::string filename, long firstLabelIndex, long lastLabelIndex) {
    std::ifstream file{filename};
    auto elementCount = readHeader(file, filename, MAGIC_NUMBER_MSB_FIRST, firstLabelIndex, lastLabelIndex);

    char byte;
    for (int i = 0; i < firstLabelIndex; ++i) {
        if (not file) {
            throw MnistException{"unexpected end of file!"};
        }
        file.get(byte);
    }

    auto requestedImageCount = lastLabelIndex - firstLabelIndex;
    requestedImageCount += lastLabelIndex >= 0 ? 0 : elementCount; // using negative lastLabelIndex means cyclic indexes

    std::vector<Byte> labels;

    for (int i = 0; i < requestedImageCount; ++i) {
        if (not file) {
            throw MnistException{"unexpected end of file!"};
        }
        file.get(byte);
        labels.push_back(byte);
    }

    return labels;
}

std::string printAscii(const Image &image) {
    std::string palette = " .-:/1O8#";
    std::stringstream ss;
    int i = 0;
    for (size_t h = 0; h < image.height; ++h) {
        for (size_t w = 0; w < image.width; ++w) {
            unsigned int pixel = image.pixels[i++];
            unsigned int color = pixel >> 5u;
            ss << palette[color];
        }
        ss << "\n";
    }
    return ss.str();
}

