/**
 * @file Input.h
 * @date 2021-11-06
 */

#ifndef DYSINET_INPUT_H
#define DYSINET_INPUT_H

using Byte = unsigned char;
using Datum = uint32_t;
using Input = std::vector<Datum>;


#endif //DYSINET_INPUT_H
