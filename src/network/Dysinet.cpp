
#include <sstream>
#include <cmath>
#include <queue>
#include "commonlibs/StackTracedException.h"
#include "commonlibs/Walkers.h"
#include "Dysinet.h"

using randomize::exception::StackTracedException;
using randomize::string::container_to_string;

bool inRange(long elem) {
    return elem >= 0 and elem < 256;
}

Input parse(const std::string& line) {
    std::stringstream ss{line};
    Input input;
    long elem;
    while (ss >> elem) {
        if (not inRange(elem)) {
            throw StackTracedException{"value doesn't fit in a Byte: " + std::to_string(elem)};
        }
        input.push_back(elem);
    }
    return input;
}

std::string to_string(const Input &input) {
    return container_to_string(input,
                               [](Byte b) {
                                   return static_cast<long>(b);
                               });
}

std::string to_string(const Output &output) {
    return std::to_string(output);
}

std::string to_string(const Mapping &mapping) {
    auto res = container_to_string(mapping, [](auto elem) {
        const auto &res =
                "(" + to_string(elem.first) + ", "
                + to_string(elem.second) + ")";
        return res;
    });
    return res;
}

/**
 * Entropy: sum_i(p_i * log(p_i)).
 * Having (T=sum_i(n_i)) and (p_i = n_i/T), then an alternate expression for entropy is:
 * E = (sum_i(n_i * log(n_i))/T - log(T))
 */
template<typename T>
double measureInformationT(std::map<T, long>  historic) {
    long totatCount = 0;
    double entropyAccum = 0;
    for (const auto &[input, count] : historic) {
        (void)input; // silence warning about unused variable. [[maybe_unused]] doesn't work in g++ 7.5
        totatCount += count;
        entropyAccum += count * log2(count);
    }
    auto entropy = entropyAccum / totatCount - log2(totatCount);
    return std::abs(entropy);
}

// TODO optimize
double measureInformation(Historic historic) {
    return measureInformationT(historic);
}

void measureCorrelation(const std::vector<Input> &inputs, size_t inputIndex,
                        std::vector<std::vector<double>> &correlations) {
    for (size_t correlatedTo = 0; correlatedTo < inputs[inputIndex].size(); ++correlatedTo) {
        if (inputIndex == correlatedTo) {
            continue;
        }
        std::map<int, std::map<int, int>> historic;
        for (const auto &input : inputs) {
            historic[input[inputIndex]][input[correlatedTo]]++;
        }
        std::map<int, long> correlation;
        for (const auto &[a, b] : historic) {
            (void)a; // silence warning about unused variable. [[maybe_unused]] doesn't work in g++ 7.5
            for (const auto &[ba, bb] : b) {
                (void)ba; // silence warning about unused variable. [[maybe_unused]] doesn't work in g++ 7.5
                correlation[b.size()] += bb;
            }
        }
        long totalCount = 0;
        long equivalentCount = 0;
        for (const auto &[diversity, count] : correlation) {
            totalCount += count;
            if (diversity == 1) {
                equivalentCount += count;
            }
        }
        auto info = equivalentCount / (double)totalCount;
        correlations[inputIndex][correlatedTo] = info;
    }
}

std::vector<std::vector<double>> measureCorrelation(std::vector<Input> inputs) {
    if (inputs.empty()) {
        throw StackTracedException{"can't measure correlation on an empty list!"};
    }
    std::vector<std::vector<double>> correlations;
    auto inputSize = inputs[0].size();
    correlations.resize(inputSize);
    for (size_t inputIndex = 0; inputIndex < inputSize; ++inputIndex) {
        correlations[inputIndex].resize(inputSize);
        measureCorrelation(inputs, inputIndex, correlations);
    }
    return correlations;
}
std::vector<std::vector<double>> measureSymmetricCorrelation(std::vector<Input> inputs) {
    auto correlation = measureCorrelation(inputs);
    std::vector<std::vector<double>> symmetric;
    symmetric.resize(correlation.size());
    for (size_t a = 0; a < correlation.size(); ++a) {
        symmetric[a].resize(correlation.size());
    }
    for (size_t a = 0; a < correlation.size(); ++a) {
        for (size_t b = a; b < correlation.size(); ++b) {
            symmetric[a][b] = symmetric[b][a] = std::min(correlation[a][b], correlation[b][a]);
        }
    }
    return symmetric;
}

long measureDistance(const Input &input1, const Input &input2, long distanceToStop) {
    size_t size1 = input1.size();
    if (size1 != input2.size()) {
        throw StackTracedException{"can't measure distance of inputs of different size! ("
                                   + std::to_string(input1.size()) + " and " + std::to_string(input2.size()) + "): "
                                   + container_to_string(input1) + ", " + container_to_string(input2)};
    }
    size_t unrolledLoopDefaultLength = size1*13/16; // more than half, but before waiting for the end. see optimizing.txt
    auto count = 0L;
    for (size_t i = 0; i < size1; ++i) {
        size_t remainingIterations = size1 - i;
        auto unrollableLoop = std::min(remainingIterations, unrolledLoopDefaultLength);
        for (size_t j = 0; j < unrollableLoop; ++j, ++i) {
            if (input1[i] != input2[i]) {
                count++;
            }
        }
        if (count > distanceToStop) {
            return count;
        }
    }
    return count;
}
long measureDistance(const Input &input1, const Input &input2) {
    size_t size1 = input1.size();
    if (size1 != input2.size()) {
        throw StackTracedException{"can't measure distance of inputs of different size! " + container_to_string(input1)
                + ", " + container_to_string(input2)};
    }
    auto count = 0L;
    for (size_t i = 0; i < size1; ++i) {
        if (input1[i] != input2[i]) {
            count++;
        }
    }
    return count;
}


struct Compare {
     bool operator()(const std::pair<long, Label> &__x, const std::pair<long, Label>& __y) const {
         return __x.first < __y.first;
     }
};




void closestMatch(const Mapping &mapping, const Input& goal, Input &minInput, Label &minLabel, long &minDistance) {
    minDistance = goal.size() + 1;
    for (const auto &[input, label] : mapping) {
        auto d = measureDistance(input, goal, minDistance);
        if (d < minDistance) {
            minDistance = d;
            minLabel = label;
            minInput = input;
        }
    }
}
Output closestMatch(const Mapping &mapping, const Input& goal) {
    Output minOutput;
    Input minInput;
    long minDistance;
    closestMatch(mapping, goal, minInput, minOutput, minDistance);
    return minOutput;
}

Input closestInputMatch(const Mapping &mapping, const Input &goal) {
    Output minOutput;
    Input minInput;
    long minDistance;
    closestMatch(mapping, goal, minInput, minOutput, minDistance);
    return minInput;
}
long closestDistanceMatch(const Mapping &mapping, const Input &goal) {
    Output minOutput;
    Input minInput;
    long minDistance;
    closestMatch(mapping, goal, minInput, minOutput, minDistance);
    return minDistance;
}

Input vote(std::vector<Input> inputs) {
    if (inputs.empty()) {
        throw StackTracedException{"can't vote on an empty list!"};
    }
    auto length = inputs[0].size();
    Input voted;
    for (size_t i = 0; i < length; ++i) {
        std::map<Datum, long> counts;
        long mostFrequentDatum = -1;
        long count;
        long maxFrequency = 0;
        for (const auto &input : inputs) {
            count = ++(counts[input[i]]);
            if (maxFrequency < count) {
                maxFrequency = count;
                mostFrequentDatum = input[i];
            }
        }
        voted.push_back(mostFrequentDatum);
    }
    return voted;
}

