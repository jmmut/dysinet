#ifndef PIPES_DYSINET_H
#define PIPES_DYSINET_H

#include <utility>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <commonlibs/Walkers.h>
#include <commonlibs/StackTracedException.h>
#include <mnist/Input.h>


struct Input2D {
    Input input;
    std::vector<long> dimensionSizes;
    static const int DIMENSIONS = 2;
};
using Label = uint32_t;
inline const unsigned int PATTERNS_IN_LABEL = 32;
using Output = Label;
using Historic = std::map<Input, long>;
using Mapping = std::map<Input, Label>;

Input parse(const std::string& line);
std::string to_string(const Output &output);
std::string to_string(const Mapping &mapping);

double measureInformation(Historic historic);
std::vector<std::vector<double>> measureCorrelation(std::vector<Input> inputs);
std::vector<std::vector<double>> measureSymmetricCorrelation(std::vector<Input> inputs);
long measureDistance(const Input &input1, const Input &input2);
long measureDistance(const Input &input1, const Input &input2, long distanceToStop);
Output closestMatch(const Mapping &mapping, const Input& input);
Input closestInputMatch(const Mapping &mapping, const Input& input);
long closestDistanceMatch(const Mapping &mapping, const Input &goal);
Input vote(std::vector<Input> vector);



class Dysinet {
public:
    virtual ~Dysinet() {};
    virtual Output ingest(Input input) = 0;
    virtual Output guess(Input input) = 0;
    virtual Input reconstruct(Input input) = 0;

    virtual double measureInformation() = 0;
    virtual double measureSaturation() = 0;
    virtual long measureConfidence(Input input) = 0;
    virtual std::string mappingToString() = 0;
};

class AtemporalDysinet : public Dysinet {
public:
    AtemporalDysinet() {
        highestLabel = 1;
    }
    explicit AtemporalDysinet(std::vector<long> dimensionSizes) : AtemporalDysinet{} {}

    Label &createNewLabel() {
        if (highestLabel == (1u << (PATTERNS_IN_LABEL -1))) {
            throw randomize::exception::StackTracedException{"AtemporalDysinet ran out of capacity"};
        }
        highestLabel = highestLabel << 1u;
        return highestLabel;
    }

    Output ingest(Input input) override {
        auto &count = previousInputs[input];
        count++;
        if (count <= 1) {
            Label &newLabel = createNewLabel();
            mapping[input] = newLabel;
            return newLabel;
        } else {
            return mapping[input];
        }
    }

    Output guess(Input input) override {
        auto found = mapping.find(input);
        if (found != mapping.end()) {
            return found->second;
        } else {
            return closestMatch(mapping, input);
        }
    }

    Input reconstruct(Input input) override {
        auto found = mapping.find(input);
        if (found != mapping.end()) {
            return found->first;
        } else {
            return closestInputMatch(mapping, input);
        }
    }

    double measureInformation() override {
        return ::measureInformation(previousInputs);
    }

    double measureSaturation() override {
        return mapping.size();
    }

    long measureConfidence(Input input) override {
        return ::closestDistanceMatch(mapping, input);
    }

    std::string mappingToString() override {
        auto res = to_string(mapping);
        return res;
    }
private:
    Historic previousInputs;
    Mapping mapping;
    Label highestLabel;
};

class TemporalDysinet : public Dysinet {
public:
    TemporalDysinet() {
        highestLabel = 1;
        previousLabel = highestLabel;
    }
    TemporalDysinet(Label startingLabel) {
        highestLabel = 1;
        previousLabel = std::move(startingLabel);
    }

    Label &createNewLabel() {
        if (highestLabel == (1u << (PATTERNS_IN_LABEL -1))) {
            throw randomize::exception::StackTracedException{"TemporalDysinet ran out of capacity"};
        }
        highestLabel = highestLabel << 1u;
        return highestLabel;
    }

    Output ingest(Input input) override {
        auto inputAndPreviousLabel{input};
        inputAndPreviousLabel.push_back(previousLabel);
        auto &count = previousInputs[inputAndPreviousLabel];
        count++;
        if (count <= 1) {
            auto newLabel = createNewLabel();
            mapping[inputAndPreviousLabel] = newLabel;
            previousLabel = newLabel;
        } else {
            previousLabel = mapping[inputAndPreviousLabel];
        }
        return previousLabel;
    }

    Output guess(Input input) override {
        input.push_back(previousLabel);
        auto found = mapping.find(input);
        if (found != mapping.end()) {
            return found->second;
        } else {
            return closestMatch(mapping, input);
        }
    }

    Input reconstruct(Input input) override {
        return reconstruct(input, previousLabel);
    }
    Input reconstruct(Input input, Label previousLabel) {
        input.push_back(previousLabel);
        auto found = mapping.find(input);
        if (found != mapping.end()) {
            return found->first;
        } else {
            return closestInputMatch(mapping, input);
        }
    }

    Input predict(Input input) {
        Input nextInput;
        nextInput.resize(input.size());
        auto guessedLabel = guess(input);
        auto reconstructed = reconstruct(nextInput, guessedLabel);
        return reconstructed;
    }

    double measureInformation() override {
        return ::measureInformation(previousInputs);
    }

    double measureSaturation() override {
        return mapping.size();
    }

    long measureConfidence(Input input) override {
        input.push_back(previousLabel);
        return closestDistanceMatch(mapping, input);
    }

    std::string mappingToString() override {
        auto res = to_string(mapping);
        return res;
    }

private:
    Label previousLabel;
    Label highestLabel;
    Historic previousInputs;
    Mapping mapping;
};

class SquareGenerator {
public:
    SquareGenerator(const Input2D &input2D, const long stimulusWidth, const long stimulusHeight,
            long movementHorizontal, long movementVertical)
            : _input2D{input2D}, _width{stimulusWidth}, _height{stimulusHeight},
            _movementHorizontal{movementHorizontal}, _movementVertical{movementVertical},
            _lastMovementHorizontal{0}, _lastMovementVertical{0},
            _index{0},
            _hasNext{not input2D.input.empty()} {
    }

    bool hasNext() const {return _hasNext;}

    void getNext(Input &input) {
//        auto width = std::min(_input2D.dimensionSizes[0], _width);
//        auto height = std::min(_input2D.dimensionSizes[1], _height);
//        _hasNext = (start +

        if (_index < 0) {
            throw randomize::exception::StackTracedException{
                    "called getNext on a finished generator. check hasNext before calling getNext."};
        }
        input.clear();
        long cursor = _index;
        for (int i_height = 0; i_height < _height; ++i_height, cursor+=_input2D.dimensionSizes[0]) {
            input.insert(input.end(), _input2D.input.begin() + cursor, _input2D.input.begin() + cursor + _width);
        }
        _lastMovementHorizontal = 0;
        _lastMovementVertical = 0;
        long cursorInRow = _index % _input2D.dimensionSizes[0];
        auto nextCursorInRow = cursorInRow + _movementHorizontal;
        if ((nextCursorInRow + _width) > _input2D.dimensionSizes[0]) {
            nextCursorInRow = 0;
            long cursorInColumn = _index / _input2D.dimensionSizes[0];
            auto nextCursorInColumn = cursorInColumn + _movementVertical;
            if ((nextCursorInColumn + _height) > _input2D.dimensionSizes[1]) {
                _hasNext = false;
                _index = -1;
                _lastMovementHorizontal = _lastMovementVertical = 0;
            } else {
                _index = nextCursorInRow + nextCursorInColumn * _input2D.dimensionSizes[1];
                _lastMovementVertical = nextCursorInColumn - cursorInColumn;
                _lastMovementHorizontal = nextCursorInRow - cursorInRow;
            }
        } else {
            _lastMovementHorizontal = nextCursorInRow - cursorInRow;
            _index += _lastMovementHorizontal;
        }

    }

    void getLastMovement(long &lastMovementHorizontal, long &lastMovementVertical) const {
        lastMovementHorizontal = _lastMovementHorizontal;
        lastMovementVertical = _lastMovementVertical;
    }

    long getIndex() const { return _index; }

private:
    const Input2D &_input2D;
    const long _width, _height;
    const long _movementHorizontal, _movementVertical;
    long _lastMovementHorizontal, _lastMovementVertical;
    long _index;
    bool _hasNext;
};

class AttentionDysinet : public Dysinet {
public:
    AttentionDysinet() {
        init();
        previousLabel = highestLabel;
    }
    explicit AttentionDysinet(Label startingLabel) {
        init();
        previousLabel = std::move(startingLabel);
    }

    explicit AttentionDysinet(std::vector<long> dimensionSizes) : dimensionSizes(std::move(dimensionSizes)) {
        init();
        previousLabel = highestLabel;
    }

    virtual ~AttentionDysinet() {
        highestLabel = 1;
    }

    void init() {
        highestLabel = 1;
        stimulusWidth = 16;
        stimulusHeight = 16;
        horizontalMovement = 12;
        verticalMovement = 12;
    }

    Label &createNewLabel() {
        if (highestLabel == (1u << (PATTERNS_IN_LABEL -1))) {
            throw randomize::exception::StackTracedException{"AttentionDysinet ran out of capacity"};
        }
        highestLabel = highestLabel << 1u;
        return highestLabel;
    }

    Output ingest(Input input) override {
        Input inputPortion;
        Input2D input2D{std::move(input), dimensionSizes};
        SquareGenerator squareGenerator{input2D, stimulusWidth, stimulusHeight, horizontalMovement, verticalMovement};

        long lastHorizontalMovement, lastVerticalMovement;
        while (squareGenerator.hasNext()) {
            squareGenerator.getNext(inputPortion);
            squareGenerator.getLastMovement(lastHorizontalMovement, lastVerticalMovement);
            inputPortion.push_back(100 - lastHorizontalMovement);
            inputPortion.push_back(100 - lastVerticalMovement);
            ingestPortion(inputPortion);
        }
        return previousLabel;
    }

    Output ingestPortion(Input input) {
        auto inputAndPreviousLabel{std::move(input)};
        inputAndPreviousLabel.push_back(previousLabel);
        auto &count = previousInputs[inputAndPreviousLabel];
        count++;
        if (count <= 1) {
            auto newLabel = createNewLabel();
            mapping[inputAndPreviousLabel] = newLabel;
            previousLabel = newLabel;
        } else {
            previousLabel = mapping[inputAndPreviousLabel];
        }
        return previousLabel;
    }

    Output guess(Input input) override {
        input.push_back(previousLabel);
        auto found = mapping.find(input);
        if (found != mapping.end()) {
            return found->second;
        } else {
            return closestMatch(mapping, input);
        }
    }

    Input reconstruct(Input input) override {
        return reconstruct(input, previousLabel);
    }
    Input reconstruct(Input input, Label previousLabel) {
        auto originalSize = input.size();
        Input inputPortion;
        Input2D input2D{std::move(input), dimensionSizes};
        SquareGenerator squareGenerator{input2D, stimulusWidth, stimulusHeight, horizontalMovement, verticalMovement};

        Input result;
        result.resize(originalSize);
        long lastHorizontalMovement, lastVerticalMovement;
        while (squareGenerator.hasNext()) {
            auto idx = squareGenerator.getIndex();
            squareGenerator.getNext(inputPortion);
            squareGenerator.getLastMovement(lastHorizontalMovement, lastVerticalMovement);
            inputPortion.push_back(100 - lastHorizontalMovement);
            inputPortion.push_back(100 - lastVerticalMovement);
            auto reconstructed = reconstructPortion(inputPortion, previousLabel);
            reconstructed.pop_back();
            reconstructed.pop_back();
            reconstructed.pop_back();
            size_t i = 0;
            for (int i_h = 0; i_h < stimulusHeight; ++i_h) {
                for (int i_w = 0; i_w < stimulusWidth; ++i_w) {
                    result[idx + i_w + i_h * dimensionSizes[0]] = reconstructed[i++];
                }
            }
        }
        if (result.size() != originalSize) {
            throw randomize::exception::StackTracedException{"reconstruction failed! reconstructed size is "
                                                             + std::to_string(result.size()) + " but it should be "
                                                             + std::to_string(originalSize)};
        }
        return result;
    }
    Input reconstructPortion(Input input, Label previousLabel) {
        input.push_back(previousLabel);
        auto found = mapping.find(input);
        if (found != mapping.end()) {
            return found->first;
        } else {
            return closestInputMatch(mapping, input);
        }
    }

    Input predict(Input input) {
        Input nextInput;
        nextInput.resize(input.size());
        auto guessedLabel = guess(input);
        auto reconstructed = reconstruct(nextInput, guessedLabel);
        return reconstructed;
    }

    double measureInformation() override {
        return ::measureInformation(previousInputs);
    }

    double measureSaturation() override {
        return mapping.size();
    }

    long measureConfidence(Input input) override {
        Input inputPortion;
        Input2D input2D{std::move(input), dimensionSizes};
        SquareGenerator squareGenerator{input2D, stimulusWidth, stimulusHeight, horizontalMovement, verticalMovement};

        auto distanceAccumulator = 0;
        long lastHorizontalMovement, lastVerticalMovement;
        while (squareGenerator.hasNext()) {
            squareGenerator.getNext(inputPortion);
            squareGenerator.getLastMovement(lastHorizontalMovement, lastVerticalMovement);
            inputPortion.push_back(100 - lastHorizontalMovement);
            inputPortion.push_back(100 - lastVerticalMovement);
            inputPortion.push_back(previousLabel);
            distanceAccumulator += closestDistanceMatch(mapping, inputPortion);
        }
        return distanceAccumulator;
    }

    std::string mappingToString() override {
        auto res = to_string(mapping);
        return res;
    }

private:
    Label previousLabel;
    Label highestLabel;
    Historic previousInputs;
    Mapping mapping;
    long stimulusWidth, stimulusHeight;
    long horizontalMovement, verticalMovement;
    std::vector<long> dimensionSizes;
};


/**
 * The advantage of this implementation is that the last innerDysinet works on a list of Labels instead of a list of
 * Inputs, but the disadvantage is that it has to have the same saturation (number of patters) as the other
 * implementations, so it's not really scalable.
 */
class HierarchicalDysinet : public Dysinet {
public:
    Output ingest(Input input) override {
        Input input1{input.begin(), input.begin() + input.size()/2};
        Input input2{input.begin()+ input.size()/2, input.end()};

        auto output1 = inputDysinet1.ingest(input1);
        auto output2 = inputDysinet2.ingest(input2);

        Input mainInput;
        mainInput.push_back(output1);
        mainInput.push_back(output2);
        auto mainOutput = mainDysinet.ingest(mainInput);

        return mainOutput;
    }

    Output guess(Input input) override {
        Input input1{input.begin(), input.begin() + input.size()/2};
        Input input2{input.begin()+ input.size()/2, input.end()};

        auto output1 = inputDysinet1.guess(input1);
        auto output2 = inputDysinet2.guess(input2);

        Input mainInput;
        mainInput.push_back(output1);
        mainInput.push_back(output2);
        auto mainOutput = mainDysinet.guess(mainInput);

        return mainOutput;
    }

    Input reconstruct(Input input) override {
        Input input1{input.begin(), input.begin() + input.size()/2};
        Input input2{input.begin()+ input.size()/2, input.end()};

        auto output1 = inputDysinet1.reconstruct(input1);
        auto output2 = inputDysinet2.reconstruct(input2);

        output1.insert(output1.end(), output2.begin(), output2.end());

        return output1;
    }

    double measureInformation() override {
        auto info1 = inputDysinet1.measureInformation();
        auto info2 = inputDysinet2.measureInformation();
        auto mainInfo = mainDysinet.measureInformation();
        return info1 + info2 + mainInfo;
    }

    double measureSaturation() override {
        return std::max({inputDysinet1.measureSaturation(),
                         inputDysinet2.measureSaturation(),
                         mainDysinet.measureSaturation()});
    }

    long measureConfidence(Input input) override {
        throw randomize::exception::StackTracedException{"unimplemented"};
    }

    std::string mappingToString() override {
        std::string s1 = inputDysinet1.mappingToString();
        std::string s2 = inputDysinet2.mappingToString();
        std::string sm = mainDysinet.mappingToString();
        return "{i1: " + s1
               + ", i2: " + s2
               + ", main: " + sm + "}";
    }

private:
    AtemporalDysinet inputDysinet1, inputDysinet2, mainDysinet;
};

using Confidence = std::pair<long, size_t>;

struct CompareConfidence {
    bool operator()(const Confidence &__x, const Confidence & __y) const {
        return __x.first < __y.first;
    }
};

template<typename TDysinet = AtemporalDysinet>
class GrowingDysinet : public Dysinet {
public:
    explicit GrowingDysinet(std::vector<long> dimensionSizes) : dimensionSizes(std::move(dimensionSizes)) {
        init();
    }

    GrowingDysinet() {
        init();
    }

    void init() {
        dysinets.emplace_back(dimensionSizes);
    }

    Output ingest(Input input) override {
        std::vector<long> distances;
        std::priority_queue<Confidence,
                std::vector<Confidence>,
                CompareConfidence> priorityQueue;
        size_t index = 0;
        for (auto &dysinet : dysinets) {
            distances.push_back(dysinet.measureConfidence(input));
            priorityQueue.push(Confidence{distances.back(), index++});
        }
        Confidence firstMin = priorityQueue.top();
        Confidence min = firstMin;
        while ((not priorityQueue.empty()) and dysinets[min.second].measureSaturation() >= 27) {
            priorityQueue.pop();
            min = priorityQueue.top();
        }
        if (priorityQueue.empty()) {
            dysinets.emplace_back(dimensionSizes);
            dysinets.back().ingest(input);
        } else {
            dysinets[min.second].ingest(input);
        }
        return 0; // TODO this class should really return vector<Output>
    }

    Output guess(Input input) override {
        std::vector<long> distances;
        for (auto &dysinet : dysinets) {
            distances.push_back(dysinet.measureConfidence(input));
        }
        auto it_min = std::min_element(distances.begin(), distances.end());
        if (it_min.operator*() == 0) {
            // someone knows exactly this pattern
            return dysinets[it_min - distances.begin()].guess(input);
        }

        Output output = 0;
        for (auto &dysinet : dysinets) {
            output |= dysinet.guess(input);
        }
        return output;
    }

    Input reconstruct(Input input) override {
        std::vector<long> distances;
        for (auto &dysinet : dysinets) {
            distances.push_back(dysinet.measureConfidence(input));
        }
        auto it_min = std::min_element(distances.begin(), distances.end());
        if (it_min.operator*() == 0) {
            // someone knows exactly this pattern
            return dysinets[it_min - distances.begin()].reconstruct(input);
        }

        std::vector<Input> reconstructed;
        for (size_t i = 0; i < distances.size(); ++i) {
            if (distances[i] == it_min.operator*()) { // vote only the ones that are closest
                reconstructed.push_back(dysinets[i].reconstruct(input));
            }
        }
        auto voted = vote(reconstructed);
        return voted;
    }

    double measureInformation() override {
        double info = 0;
        for (auto &dysinet : dysinets) {
            info += dysinet.measureInformation();
        }
        return info;
    }

    double measureSaturation() override {
        double saturation = 0;
        for (auto &dysinet : dysinets) {
            saturation += dysinet.measureSaturation();
        }
        return saturation;
    }

    long measureConfidence(Input input) override {
        throw randomize::exception::StackTracedException{"unimplemented"};
    }

    std::string mappingToString() override {
        auto i = 0;
        return randomize::string::container_to_string(dysinets, [&](auto dysinet) {
            return "d" + std::to_string(i++) + ": " + dysinet.mappingToString();
        });
    }

private:
    std::vector<long> dimensionSizes;
    std::vector<TDysinet> dysinets;
};


#endif //PIPES_DYSINET_H
