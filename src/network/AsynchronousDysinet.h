/**
 * @file AsynchronousDysinet.h
 * @date 2021-11-06
 */

#ifndef DYSINET_ASYNCHRONOUSDYSINET_H
#define DYSINET_ASYNCHRONOUSDYSINET_H


#include "network/Dysinet.h"

class Neuron {
public:
    explicit Neuron(int inputCount);
private:
    std::vector<uint32_t> weights;
};

using Status = std::vector<Datum>;

class AsynchronousDysinet {
public:
    explicit AsynchronousDysinet(int neuronCount, int inputCount);

    void perceive(Input input);
    Status getStatus();
private:
    std::vector<Neuron> neurons;
};


#endif //DYSINET_ASYNCHRONOUSDYSINET_H
