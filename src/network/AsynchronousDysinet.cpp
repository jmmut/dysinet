/**
 * @file AsynchronousDysinet.cpp
 * @date 2021-11-06
 */

#include "AsynchronousDysinet.h"

Neuron::Neuron(int inputCount) : weights(inputCount, 0u) {}

AsynchronousDysinet::AsynchronousDysinet(int neuronCount, int inputCount)
        : neurons(neuronCount, Neuron{inputCount}) {
}

void AsynchronousDysinet::perceive(Input input) {

}

Status AsynchronousDysinet::getStatus() {
    return {};
}
