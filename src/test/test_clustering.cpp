

#include <iostream>
#include <cmath>
#include <network/Dysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"
#include "commonlibs/StackTracedException.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::string::container_to_string;

bool floatEquals(double actual, double expected) {
    auto delta = std::pow(2, -10);
    auto diff = std::abs(actual - expected);
    return diff < delta;
}

std::string messageFloatsAreNotEqual(double actual, double expected) {
    return std::to_string(actual) + " is not equal to " + std::to_string(expected);
}

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"clustering", {
            []() {
            }
    }}.countFailed();

    std::vector<Input> correlatedInputs{
            {10, 20},
            {11, 21},
            {12, 22},
            {13, 23},
            {14, 24},
            {15, 25},
            {16, 26},
            {17, 27},
            {18, 28},
            {19, 29},
    };
    std::vector<Input> independentInputs{
            {10, 20},
            {11, 21},
            {12, 22},
            {13, 20},
            {14, 21},
            {10, 22},
            {11, 20},
            {12, 21},
            {13, 22},
            {14, 29},
    };
    std::vector<Input> mixedInputs{
            {10, 20},
            {11, 21},
            {12, 22},
            {13, 23},
            {14, 24},
            {15, 25},
            {16, 26},
            {17, 23},
            {18, 24},
            {19, 25},
    };
    failed += TestSuite{"correlation", {
            [&]() {
                auto correlation = measureCorrelation(correlatedInputs);
                double ab = correlation[0][1];
                double ba = correlation[1][0];
                ASSERT_OR_THROW_MSG(floatEquals(ab, ba), messageFloatsAreNotEqual(ab, ba));
                ASSERT_OR_THROW_MSG(floatEquals(ab, 1), messageFloatsAreNotEqual(ab, 1));
            },
            [&]() {
                auto correlation = measureCorrelation(independentInputs);
                double ab = correlation[0][1];
                double ba = correlation[1][0];
                ASSERT_OR_THROW_MSG(floatEquals(ab, 0), messageFloatsAreNotEqual(ab, 0));
                ASSERT_OR_THROW_MSG(floatEquals(ba, 0.1), messageFloatsAreNotEqual(ba, 0.1));
            },
            [&]() {
                auto correlation = measureCorrelation(mixedInputs);
                double ab = correlation[0][1];
                double ba = correlation[1][0];
                ASSERT_OR_THROW_MSG(floatEquals(ab, 1), messageFloatsAreNotEqual(ab, 1));
                ASSERT_OR_THROW_MSG(floatEquals(ba, 0.4), messageFloatsAreNotEqual(ba, 0.4));
            }
    }}.countFailed();

    failed += TestSuite{"symmetric correlation", {
            [&]() {
                auto correlation = measureSymmetricCorrelation(correlatedInputs);
                double ab = correlation[0][1];
                double ba = correlation[1][0];
                ASSERT_OR_THROW_MSG(floatEquals(ab, ba), messageFloatsAreNotEqual(ab, ba));
                ASSERT_OR_THROW_MSG(floatEquals(ab, 1), messageFloatsAreNotEqual(ab, 1));
            },
            [&]() {
                auto correlation = measureSymmetricCorrelation(independentInputs);
                double ab = correlation[0][1];
                double ba = correlation[1][0];
                ASSERT_OR_THROW_MSG(floatEquals(ab, ba), messageFloatsAreNotEqual(ab, ba));
                ASSERT_OR_THROW_MSG(floatEquals(ab, 0), messageFloatsAreNotEqual(ab, 0));
            },
            [&]() {
                auto correlation = measureSymmetricCorrelation(mixedInputs);
                double ab = correlation[0][1];
                double ba = correlation[1][0];
                ASSERT_OR_THROW_MSG(floatEquals(ab, ba), messageFloatsAreNotEqual(ab, ba));
                ASSERT_OR_THROW_MSG(floatEquals(ba, 0.4), messageFloatsAreNotEqual(ba, 0.4));
            }
    }}.countFailed();

    failed += TestSuite{"vote", {
            [&]() {
                std::vector<Input> inputs{
                        {10, 20},
                        {10, 22},
                        {12, 22},
                        {13, 23},
                };
                auto voted = vote(inputs);
                ASSERT_EQUALS_OR_THROW(voted[0], 10);
                ASSERT_EQUALS_OR_THROW(voted[1], 22);
            }
    }}.countFailed();

    failed += TestSuite{"square generator", {
            [&]() {
                Input input{1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 6, 6, 4, 4, 8, 8};
                Input2D input2D{input, {4, 4}};
                long mh = 1, mv = 2;
                long actualMh, actualMv;
                SquareGenerator generator{input2D, 3, 2, mh, mv};
                ASSERT_OR_THROW(generator.hasNext());
                Input square;

                generator.getLastMovement(actualMh, actualMv);
                ASSERT_EQUALS_OR_THROW(actualMh, 0);
                ASSERT_EQUALS_OR_THROW(actualMv, 0);
                generator.getNext(square);
                ASSERT_EQUALS_OR_THROW(square.size(), 6);
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(square), "[1, 1, 1, 2, 2, 2]");

                generator.getLastMovement(actualMh, actualMv);
                ASSERT_EQUALS_OR_THROW(actualMh, 1);
                ASSERT_EQUALS_OR_THROW(actualMv, 0);
                generator.getNext(square);
                ASSERT_EQUALS_OR_THROW(square.size(), 6);
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(square), "[1, 1, 1, 2, 2, 2]");

                generator.getLastMovement(actualMh, actualMv);
                ASSERT_EQUALS_OR_THROW(actualMh, -1);
                ASSERT_EQUALS_OR_THROW(actualMv, 2);
                generator.getNext(square);
                ASSERT_EQUALS_OR_THROW(square.size(), 6);
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(square), "[3, 3, 6, 4, 4, 8]");

                generator.getLastMovement(actualMh, actualMv);
                ASSERT_EQUALS_OR_THROW(actualMh, 1);
                ASSERT_EQUALS_OR_THROW(actualMv, 0);
                generator.getNext(square);
                ASSERT_EQUALS_OR_THROW(square.size(), 6);
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(square), "[3, 6, 6, 4, 8, 8]");

                generator.getLastMovement(actualMh, actualMv);
                ASSERT_EQUALS_OR_THROW(actualMh, 0);
                ASSERT_EQUALS_OR_THROW(actualMv, 0);
            }
    }}.countFailed();

    return failed > 0;
}
