/**
 * @file test_reinforcement.cpp
 * @date 2021-11-01
 */

#include <network/Dysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"


using randomize::test::TestSuite;

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;
    failed += TestSuite{"basic reinforcement", {
            []() {
                ASSERT_EQUALS_OR_THROW(3, 4);
            }
    }}.countFailed();

    return failed > 0;
}
