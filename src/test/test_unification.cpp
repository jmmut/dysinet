

#include <iostream>
#include <cmath>
#include <chrono>
#include <network/Dysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::string::container_to_string;
using namespace std::chrono;

bool floatEquals(double actual, double expected) {
    auto delta = std::pow(2, -10);
    auto diff = std::abs(actual - expected);
    return diff < delta;
}

std::string messageFloatsAreNotEqual(double actual, double expected) {
    return std::to_string(actual) + " is not equal to " + std::to_string(expected);
}

Input generateInput(long pos, long size, long copies, long offset) {
    Input input;
    for (long copyIndex = 0; copyIndex < copies; ++copyIndex) {
        for (int sizeIndex = 0; sizeIndex < size; ++sizeIndex) {
            if (sizeIndex == pos + (copies - copyIndex -1)*offset) {
                input.push_back(copyIndex * 2 + 2);
            } else {
                input.push_back(copyIndex * 2 + 1);
            }
        }
    }
    return input;
}

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;
    failed += TestSuite{"generate input", {
            []() {
                auto input = generateInput(0, 4, 2, 1);
                Input expected{1, 2, 1, 1, 4, 3, 3, 3};
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(input), container_to_string(expected));
            },
            []() {
                auto input = generateInput(1, 4, 2, 1);
                Input expected{1, 1, 2, 1, 3, 4, 3, 3};
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(input), container_to_string(expected));
            },
            []() {
                auto input = generateInput(1, 6, 3, 2);
                Input expected{1, 1, 1, 1, 1, 2, 3, 3, 3, 4, 3, 3, 5, 6, 5, 5, 5, 5};
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(input), container_to_string(expected));
            }
    }}.countFailed();

    failed += TestSuite{"unification", {
            []() {
                AtemporalDysinet dysinet;
                auto output = dysinet.ingest({1, 2, 1, 1, 4, 3, 3, 3});
                Output expected = 2;
                ASSERT_EQUALS_OR_THROW(output, expected);
                output = dysinet.ingest({1, 1, 2, 1, 3, 4, 3, 3});
                expected = 4;
                ASSERT_EQUALS_OR_THROW(output, expected);
                output = dysinet.ingest({1, 1, 1, 2, 3, 3, 4, 3});
                expected = 8;
                ASSERT_EQUALS_OR_THROW(output, expected);


                auto begin = steady_clock::now();
                auto guess = dysinet.guess({1, 1, 1, 1, 3, 4, 3, 3});
                auto end = steady_clock::now();

                LOG_WARN("small guess of Atemporal: %ld µs", duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(guess, 4);
            },
            []() {
                HierarchicalDysinet dysinet;
                auto output = dysinet.ingest({1, 2, 1, 1, 4, 3, 3, 3});
                Output expected = 2;
                ASSERT_EQUALS_OR_THROW(output, expected);
                output = dysinet.ingest({1, 1, 2, 1, 3, 4, 3, 3});
                expected = 4;
                ASSERT_EQUALS_OR_THROW(output, expected);
                output = dysinet.ingest({1, 1, 1, 2, 3, 3, 4, 3});
                expected = 8;
                ASSERT_EQUALS_OR_THROW(output, expected);


                auto begin = steady_clock::now();
                auto guess = dysinet.guess({1, 1, 1, 1, 3, 4, 3, 3});
                auto end = steady_clock::now();

                LOG_WARN("small guess of Hierarchical: %ld µs", duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(guess, 4);
            },
            []() {
                AtemporalDysinet dysinet;
                Output output;
                Output expected = 1u;
                int size = 10;
                for (int i = 0; i < std::min(30, size); ++i) {
                    output = dysinet.ingest(generateInput(i, size, 2, 1));
                    expected = expected << 1u;
                    ASSERT_EQUALS_OR_THROW(output, expected);

                }
                Input inputWithErrors = generateInput(1, size, 2, 1);
                inputWithErrors[2] = 1;
                auto begin = steady_clock::now();
                auto guess = dysinet.guess(inputWithErrors);
                auto end = steady_clock::now();

                LOG_WARN("medium guess of Atemporal: %ld µs", duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(guess, 1u << 2u);
            },
            []() {
                HierarchicalDysinet dysinet;
                Output output;
                Output expected = 1u;
                int size = 10;
                for (int i = 0; i < std::min(30, size); ++i) {
                    output = dysinet.ingest(generateInput(i, size, 2, 1));
                    expected = expected << 1u;
                    ASSERT_EQUALS_OR_THROW(output, expected);

                }
                Input inputWithErrors = generateInput(1, size, 2, 1);
                inputWithErrors[2] = 1;
                auto begin = steady_clock::now();
                auto guess = dysinet.guess(inputWithErrors);
                auto end = steady_clock::now();

                LOG_WARN("medium guess of Hierarchical: %ld µs", duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(guess, 1u << 2u);
            },
            []() {
                AtemporalDysinet dysinet;
                Output output;
                Output expected = 1u;
                int size = 1000;
                for (int i = 0; i < 30; ++i) {
                    output = dysinet.ingest(generateInput(i, size, 2, 1));
                    expected = expected << 1u;
                    ASSERT_EQUALS_OR_THROW(output, expected);

                }
                Input inputWithErrors = generateInput(1, size, 2, 1);
                inputWithErrors[2] = 1;
                auto begin = steady_clock::now();
                auto guess = dysinet.guess(inputWithErrors);
                auto end = steady_clock::now();

                LOG_WARN("big guess of Atemporal: %ld µs", duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(guess, 1u << 2u);
            },
            []() {
                HierarchicalDysinet dysinet;
                Output output;
                Output expected = 1u;
                int size = 1000;
                for (int i = 0; i < 30; ++i) {
                    output = dysinet.ingest(generateInput(i, size, 2, 1));
                    expected = expected << 1u;
                    ASSERT_EQUALS_OR_THROW(output, expected);

                }
                Input inputWithErrors = generateInput(1, size, 2, 1);
                inputWithErrors[2] = 1;
                auto begin = steady_clock::now();
                auto guess = dysinet.guess(inputWithErrors);
                auto end = steady_clock::now();

                LOG_WARN("big guess of Hierarchical: %ld µs", duration_cast<microseconds>(end - begin).count());
                ASSERT_EQUALS_OR_THROW(guess, 1u << 2u);
            }
    }}.countFailed();

    return failed > 0;
}
