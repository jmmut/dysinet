

#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <network/Dysinet.h>
#include <network/AsynchronousDysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"

#include "mnist/MnistLoader.h"

using randomize::test::TestSuite;
using randomize::exception::StackTracedException;
using randomize::string::container_to_string;
using namespace std::chrono;

bool didAnyTestSuiteFail(std::initializer_list<TestSuite> suites) {
    unsigned long failed = 0;
    for (const auto &suite: suites) {
        failed += suite.countFailed();
    }
    return failed > 0;
}

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    return didAnyTestSuiteFail({
            TestSuite{"basic async", {
                []() {
                    auto dysinet = AsynchronousDysinet{20, 4};
                    dysinet.perceive(Input{0, 0, 0, 10, 50, 10, 0, 0, 0, 0, 0});
                    auto output = dysinet.getStatus();
                    std::cout << randomize::string::container_to_string(output) << std::endl;
                }
//            }},
//            TestSuite{"more async", {
//
            }}
    });
#if 0
    unsigned long failed = 0;
    failed += TestSuite{"naive check", {
        /*
            [&]() {
                std::vector<double> accuracies;
                auto tests = 1;
                for (int k = 0; k < tests; ++k) {
                    auto n = 1000u;
                    auto start = n * k;
                    auto images = loadImageRange("./resources/train-images-idx3-ubyte", start, start + n);
                    auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", start, start + n);
                    ASSERT_EQUALS_OR_THROW(images.size(), n);
                    AtemporalDysinet dysinet;
                    for (size_t i = 0; i < n; ++i) {
                        auto input{images[i].pixels};
                        input.push_back(labels[i]);
                        dysinet.ingest(input);
                        LOG_DEBUG("ingesting label %d", labels[i]);
                        LOG_DEBUG("ingesting image %s", printAscii(images[i]).c_str());
                    }

                    auto test = 0u;
                    int testSize = 100;
                    auto testImages = loadImageRange("./resources/t10k-images-idx3-ubyte", test, test + testSize);
                    auto testLabels = loadLabelRange("./resources/t10k-labels-idx1-ubyte", test, test + testSize);

                    auto correct = 0;
                    for (int j = 0; j < testSize; ++j) {
                        Input input{testImages[j].pixels};
                        input.push_back(100);
                        auto guessed = dysinet.reconstruct(input);
                        auto guessedLabel = guessed.back();
                        unsigned char &correctLabel = testLabels[j];
                        correct += correctLabel == guessedLabel;
                    }
                    accuracies.push_back(correct / (double) testSize);
                    LOG_WARN("accuracy: %f/1", accuracies.back());
                }

                auto[avg, normalisedStdDev] = standardDeviation(accuracies);
                LOG_WARN("total accuracy avg: %f, stddev: %f", avg, normalisedStdDev);
            },
            */
            [&]() {
                std::vector<double> accuracies;
                auto tests = 1;
                for (int k = 0; k < tests; ++k) {
                    auto n = 1000u;
                    auto start = n * k;
                    auto images = loadImageRange("./resources/train-images-idx3-ubyte", start, start + n);
                    auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", start, start + n);
                    ASSERT_EQUALS_OR_THROW(images.size(), n);
                    GrowingDysinet dysinet;
                    for (size_t i = 0; i < n; ++i) {
                        auto input{images[i].pixels};
                        input.push_back(labels[i]);
                        dysinet.ingest(input);
//                        LOG_DEBUG("GrowingDysinet: ingesting label %d", labels[i]);
//                        LOG_DEBUG("GrowingDysinet: ingesting image %s", printAscii(images[i]).c_str());
                    }

                    auto test = 0u;
                    int testSize = 100;
                    auto testImages = loadImageRange("./resources/t10k-images-idx3-ubyte", test, test + testSize);
                    auto testLabels = loadLabelRange("./resources/t10k-labels-idx1-ubyte", test, test + testSize);

                    auto correct = 0;
                    for (int j = 0; j < testSize; ++j) {
                        Input input{testImages[j].pixels};
                        input.push_back(100);
                        auto guessed = dysinet.reconstruct(input);
                        auto guessedLabel = guessed.back();
                        unsigned char &correctLabel = testLabels[j];
                        correct += correctLabel == guessedLabel;
                        LOG_DEBUG("GrowingDysinet: reconstructed label %d, predicted %d", testLabels[j], guessedLabel);
                        LOG_DEBUG("GrowingDysinet: reconstructed image %s", printAscii(testImages[j]).c_str());
                    }
                    accuracies.push_back(correct / (double) testSize);
                    LOG_WARN("GrowingDysinet: accuracy: %f/1", accuracies.back());
                }

                LOG_WARN("GrowingDysinet: total accuracy avg: %f", accuracies.back());
            },
            // */
            [&]() {
                std::vector<double> accuracies;
                auto tests = 1;
                for (int k = 0; k < tests; ++k) {
                    auto n = 1000u;
                    auto start = n * k;
                    auto images = loadImageRange("./resources/train-images-idx3-ubyte", start, start + n);
                    auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", start, start + n);
                    ASSERT_EQUALS_OR_THROW(images.size(), n);
                    GrowingDysinet<AttentionDysinet> dysinet{{static_cast<long>(images[0].width),
                                                              static_cast<long>(images[0].height)}};
                    for (size_t i = 0; i < n; ++i) {
                        auto input{images[i].pixels};
                        input.pop_back(); //TODO handle non-square input
                        input.push_back(labels[i]);
                        dysinet.ingest(input);
//                        LOG_DEBUG("GrowingDysinet<Attention>: ingesting label %d", labels[i]);
//                        LOG_DEBUG("GrowingDysinet<Attention>: ingesting image %s", printAscii(images[i]).c_str());
                    }

                    auto test = 0u;
                    int testSize = 100;
                    auto testImages = loadImageRange("./resources/t10k-images-idx3-ubyte", test, test + testSize);
                    auto testLabels = loadLabelRange("./resources/t10k-labels-idx1-ubyte", test, test + testSize);

                    auto correct = 0;
                    for (int j = 0; j < testSize; ++j) {
                        Input input{testImages[j].pixels};
//                        input.push_back(100); //TODO handle non-square input
                        auto guessed = dysinet.reconstruct(input);
                        auto guessedLabel = guessed.back();
                        unsigned char &correctLabel = testLabels[j];
                        correct += correctLabel == guessedLabel;
                        LOG_DEBUG("GrowingDysinet<Attention>: reconstructed label %d, predicted %d", testLabels[j], guessedLabel);
                        LOG_DEBUG("GrowingDysinet<Attention>: reconstructed image %s", printAscii(testImages[j]).c_str());
                    }
                    accuracies.push_back(correct / (double) testSize);
                    LOG_WARN("GrowingDysinet<Attention>: accuracy: %f/1", accuracies.back());
                }

                LOG_WARN("GrowingDysinet<Attention>: total accuracy avg: %f", accuracies.back());
            },
    }}.countFailed();

    return failed > 0;
#endif
}
