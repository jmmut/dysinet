

#include <iostream>
#include <cmath>
#include <network/Dysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::string::container_to_string;

bool floatEquals(double actual, double expected) {
    auto delta = std::pow(2, -10);
    auto diff = std::abs(actual - expected);
    return diff < delta;
}

std::string messageFloatsAreNotEqual(double actual, double expected) {
    return std::to_string(actual) + " is not equal to " + std::to_string(expected);
}

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;

    failed += TestSuite{"measure information", {
            []() {
                AtemporalDysinet dysinet;
                dysinet.ingest({1, 2, 3});
                auto info = dysinet.measureInformation();
                ASSERT_OR_THROW(floatEquals(info, 0.0));
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({1, 2, 3});
                info = dysinet.measureInformation();
                double expected = 0.0;
                ASSERT_OR_THROW_MSG(floatEquals(info, expected), messageFloatsAreNotEqual(info, expected));
            },
            []() {
                AtemporalDysinet dysinet;
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({2, 3, 4});
                auto info = dysinet.measureInformation();
                double expected = 1.0;
                ASSERT_OR_THROW_MSG(floatEquals(info, expected), messageFloatsAreNotEqual(info, expected));
            },
            []() {
                AtemporalDysinet dysinet;
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({2, 3, 4});
                dysinet.ingest({2, 3, 4});
                dysinet.ingest({3, 4, 5});
                dysinet.ingest({4, 5, 6});
                auto info = dysinet.measureInformation();
                ASSERT_OR_THROW(floatEquals(info, 1.75));
                double expected = 1.75;
                ASSERT_OR_THROW_MSG(floatEquals(info, expected), messageFloatsAreNotEqual(info, expected));
            },
            []() {
                AtemporalDysinet dysinet;
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({2, 3, 4});
                dysinet.ingest({2, 3, 4});
                auto expected = dysinet.measureInformation();

                dysinet.ingest({1, 2, 3});
                dysinet.ingest({1, 2, 3});
                dysinet.ingest({2, 3, 4});
                dysinet.ingest({2, 3, 4});
                dysinet.ingest({2, 3, 4});
                dysinet.ingest({2, 3, 4});
                double info = dysinet.measureInformation();

                // keeping the same proportion 1:2 should give the same entropy
                ASSERT_OR_THROW_MSG(floatEquals(info, expected), messageFloatsAreNotEqual(info, expected));
            }
    }}.countFailed();


    failed += TestSuite{"temporal dysinet", {
            []() {
                TemporalDysinet dysinet;
                auto output = dysinet.ingest({1, 2, 3});
                Output expected = 2;
                ASSERT_EQUALS_OR_THROW(output, expected);

                output = dysinet.ingest({4, 5, 6});
                expected = 4;
                ASSERT_EQUALS_OR_THROW(output, expected);

                output = dysinet.ingest({1, 2, 3});
                expected = 8;
                ASSERT_EQUALS_OR_THROW(output, expected);
            },
            []() {
                Output initialLabel;
                initialLabel = 4;
                TemporalDysinet dysinet{initialLabel};

                auto output = dysinet.ingest({1, 2, 3});
                Output expected = 2;
                ASSERT_EQUALS_OR_THROW(output, expected);

                output = dysinet.ingest({4, 5, 6});
                ASSERT_EQUALS_OR_THROW(output, initialLabel);

                output = dysinet.ingest({1, 2, 3});
                ASSERT_EQUALS_OR_THROW(output, expected);
            }
    }}.countFailed();

    failed += TestSuite{"atemporal dysinet", {
            []() {
                AtemporalDysinet dysinet;
                auto output = dysinet.ingest({1, 2, 8});
                Output expected = 2;
                ASSERT_EQUALS_OR_THROW(output, expected);
                output = dysinet.ingest({4, 5, 6});
                Output expected2 = 4;
                ASSERT_EQUALS_OR_THROW(output, expected2);

                output = dysinet.ingest({1, 2, 3});
                ASSERT_EQUALS_OR_THROW(output, expected);

                output = dysinet.ingest({4, 5, 6});
                ASSERT_EQUALS_OR_THROW(output, expected2);
            }
    }}.countFailed();

    failed += TestSuite{"growing dysinet", {
            []() {
                GrowingDysinet dysinet;
                for (uint32_t i = 1; i < 70; ++i) {
                    dysinet.ingest({i, i * 2, 3});
                }
                auto reconstructed = dysinet.reconstruct({38, 0, 3});
                Input expected{38, 76, 3};
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(reconstructed), container_to_string(expected));
            }, []() {
                GrowingDysinet<AttentionDysinet> dysinet{{4, 4}};
                Input input;
                for (uint32_t i = 1; i < 70; ++i) {
                    for (int j = 0; j < 16; ++j) {
                        input.push_back(j);
                    }
                    dysinet.ingest(input);
                }
                auto reconstructed = dysinet.reconstruct(Input{16, 50});
                Input expected{input};
                auto reconstructedString = container_to_string(reconstructed);
                auto expectedString = container_to_string(expected);
                bool eq = reconstructedString == expectedString;
                ASSERT_EQUALS_OR_THROW_SS(eq, true);
            }
    }}.countFailed();

    return failed > 0;
}
