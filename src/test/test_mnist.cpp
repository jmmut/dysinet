

#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <network/Dysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"

#include "mnist/MnistLoader.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::exception::StackTracedException;
using randomize::string::container_to_string;
using namespace std::chrono;

bool floatEquals(double actual, double expected) {
    auto delta = std::pow(2, -10);
    auto diff = std::abs(actual - expected);
    return diff < delta;
}

std::string messageFloatsAreNotEqual(double actual, double expected) {
    return std::to_string(actual) + " is not equal to " + std::to_string(expected);
}

auto average = [](const auto &nums) {
    if (nums.size() < 1) {
        throw std::logic_error{"can't compute a standard deviation of a sequence of 0 elements"};
    }
    double init = 0.0;
    auto sum = std::accumulate(nums.begin(), nums.end(), init);
    auto avg = sum / nums.size();
    return avg;
};

auto standardDeviation = [](const auto &nums) {
    if (nums.size() < 1) {
        throw std::logic_error{"can't compute a standard deviation of a sequence of 0 elements"};
    }
    auto avg = average(nums);
    double init = 0.0;
    auto stdDev = std::accumulate(nums.begin(), nums.end(), init, [=](auto accum, auto elem) {
        auto diff = elem - avg;
        accum += diff*diff;
        return accum;
    });
    auto normalisedStdDev = sqrt(stdDev / (nums.size() - 1));
    return std::pair{avg, normalisedStdDev};
};

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    unsigned long failed = 0;
    failed += TestSuite{"load image and label", {
            []() {
                auto images = loadImageRange("./resources/train-images-idx3-ubyte", 0, 1);
                ASSERT_EQUALS_OR_THROW(images.size(), 1);
                ASSERT_EQUALS_OR_THROW(images[0].pixels.size(), images[0].width * images[0].height);
            },
            []() {
                auto images = loadImageRange("./resources/train-images-idx3-ubyte", 0, 2);
                ASSERT_EQUALS_OR_THROW(images.size(), 2);
                ASSERT_EQUALS_OR_THROW(images[0].pixels.size(), images[0].width * images[0].height);
                ASSERT_EQUALS_OR_THROW(images[1].pixels.size(), images[1].width * images[1].height);
            },
            []() {
                auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", 0, 1);
                ASSERT_EQUALS_OR_THROW(labels.size(), 1);
            },
            []() {
                auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", 0, 2);
                ASSERT_EQUALS_OR_THROW(labels.size(), 2);
            },
            []() {
                auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", 0, 60000);
                ASSERT_EQUALS_OR_THROW(labels.size(), 60000);
            }
    }}.countFailed();
    failed += TestSuite{"naive check", {
        /*
            [&]() {
                std::vector<double> accuracies;
                auto tests = 1;
                for (int k = 0; k < tests; ++k) {
                    auto n = 1000u;
                    auto start = n * k;
                    auto images = loadImageRange("./resources/train-images-idx3-ubyte", start, start + n);
                    auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", start, start + n);
                    ASSERT_EQUALS_OR_THROW(images.size(), n);
                    AtemporalDysinet dysinet;
                    for (size_t i = 0; i < n; ++i) {
                        auto input{images[i].pixels};
                        input.push_back(labels[i]);
                        dysinet.ingest(input);
                        LOG_DEBUG("ingesting label %d", labels[i]);
                        LOG_DEBUG("ingesting image %s", printAscii(images[i]).c_str());
                    }

                    auto test = 0u;
                    int testSize = 100;
                    auto testImages = loadImageRange("./resources/t10k-images-idx3-ubyte", test, test + testSize);
                    auto testLabels = loadLabelRange("./resources/t10k-labels-idx1-ubyte", test, test + testSize);

                    auto correct = 0;
                    for (int j = 0; j < testSize; ++j) {
                        Input input{testImages[j].pixels};
                        input.push_back(100);
                        auto guessed = dysinet.reconstruct(input);
                        auto guessedLabel = guessed.back();
                        unsigned char &correctLabel = testLabels[j];
                        correct += correctLabel == guessedLabel;
                    }
                    accuracies.push_back(correct / (double) testSize);
                    LOG_WARN("accuracy: %f/1", accuracies.back());
                }

                auto[avg, normalisedStdDev] = standardDeviation(accuracies);
                LOG_WARN("total accuracy avg: %f, stddev: %f", avg, normalisedStdDev);
            },
            */
            [&]() {
                std::vector<double> accuracies;
                auto tests = 1;
                for (int k = 0; k < tests; ++k) {
                    auto n = 1000u;
                    auto start = n * k;
                    auto images = loadImageRange("./resources/train-images-idx3-ubyte", start, start + n);
                    auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", start, start + n);
                    ASSERT_EQUALS_OR_THROW(images.size(), n);
                    GrowingDysinet dysinet;
                    for (size_t i = 0; i < n; ++i) {
                        auto input{images[i].pixels};
                        input.push_back(labels[i]);
                        dysinet.ingest(input);
//                        LOG_DEBUG("GrowingDysinet: ingesting label %d", labels[i]);
//                        LOG_DEBUG("GrowingDysinet: ingesting image %s", printAscii(images[i]).c_str());
                    }

                    auto test = 0u;
                    int testSize = 100;
                    auto testImages = loadImageRange("./resources/t10k-images-idx3-ubyte", test, test + testSize);
                    auto testLabels = loadLabelRange("./resources/t10k-labels-idx1-ubyte", test, test + testSize);

                    auto correct = 0;
                    for (int j = 0; j < testSize; ++j) {
                        Input input{testImages[j].pixels};
                        input.push_back(100);
                        auto guessed = dysinet.reconstruct(input);
                        auto guessedLabel = guessed.back();
                        unsigned char &correctLabel = testLabels[j];
                        correct += correctLabel == guessedLabel;
                        LOG_DEBUG("GrowingDysinet: reconstructed label %d, predicted %d", testLabels[j], guessedLabel);
                        LOG_DEBUG("GrowingDysinet: reconstructed image %s", printAscii(testImages[j]).c_str());
                    }
                    accuracies.push_back(correct / (double) testSize);
                    LOG_WARN("GrowingDysinet: accuracy: %f/1", accuracies.back());
                }

                LOG_WARN("GrowingDysinet: total accuracy avg: %f", accuracies.back());
            },
            // */
            [&]() {
                std::vector<double> accuracies;
                auto tests = 1;
                for (int k = 0; k < tests; ++k) {
                    auto n = 1000u;
                    auto start = n * k;
                    auto images = loadImageRange("./resources/train-images-idx3-ubyte", start, start + n);
                    auto labels = loadLabelRange("./resources/train-labels-idx1-ubyte", start, start + n);
                    ASSERT_EQUALS_OR_THROW(images.size(), n);
                    GrowingDysinet<AttentionDysinet> dysinet{{static_cast<long>(images[0].width),
                                                              static_cast<long>(images[0].height)}};
                    for (size_t i = 0; i < n; ++i) {
                        auto input{images[i].pixels};
                        input.pop_back(); //TODO handle non-square input
                        input.push_back(labels[i]);
                        dysinet.ingest(input);
//                        LOG_DEBUG("GrowingDysinet<Attention>: ingesting label %d", labels[i]);
//                        LOG_DEBUG("GrowingDysinet<Attention>: ingesting image %s", printAscii(images[i]).c_str());
                    }

                    auto test = 0u;
                    int testSize = 100;
                    auto testImages = loadImageRange("./resources/t10k-images-idx3-ubyte", test, test + testSize);
                    auto testLabels = loadLabelRange("./resources/t10k-labels-idx1-ubyte", test, test + testSize);

                    auto correct = 0;
                    for (int j = 0; j < testSize; ++j) {
                        Input input{testImages[j].pixels};
//                        input.push_back(100); //TODO handle non-square input
                        auto guessed = dysinet.reconstruct(input);
                        auto guessedLabel = guessed.back();
                        unsigned char &correctLabel = testLabels[j];
                        correct += correctLabel == guessedLabel;
                        LOG_DEBUG("GrowingDysinet<Attention>: reconstructed label %d, predicted %d", testLabels[j], guessedLabel);
                        LOG_DEBUG("GrowingDysinet<Attention>: reconstructed image %s", printAscii(testImages[j]).c_str());
                    }
                    accuracies.push_back(correct / (double) testSize);
                    LOG_WARN("GrowingDysinet<Attention>: accuracy: %f/1", accuracies.back());
                }

                LOG_WARN("GrowingDysinet<Attention>: total accuracy avg: %f", accuracies.back());
            },
    }}.countFailed();

    return failed > 0;
}
