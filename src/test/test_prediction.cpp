

#include <iostream>
#include <cmath>
#include <chrono>
#include <network/Dysinet.h>

#include "commonlibs/SignalHandler.h"
#include "commonlibs/TestSuite.h"
#include "commonlibs/Walkers.h"

//using namespace std::string_literals;
using randomize::test::TestSuite;
using randomize::string::container_to_string;
using namespace std::chrono;

bool floatEquals(double actual, double expected) {
    auto delta = std::pow(2, -10);
    auto diff = std::abs(actual - expected);
    return diff < delta;
}

std::string messageFloatsAreNotEqual(double actual, double expected) {
    return std::to_string(actual) + " is not equal to " + std::to_string(expected);
}

Input generateInput(long pos, long size, long copies, long offset) {
    Input input;
    for (long copyIndex = 0; copyIndex < copies; ++copyIndex) {
        for (int sizeIndex = 0; sizeIndex < size; ++sizeIndex) {
            if (sizeIndex == pos + (copies - copyIndex -1)*offset) {
                input.push_back(copyIndex * 2 + 2);
            } else {
                input.push_back(copyIndex * 2 + 1);
            }
        }
    }
    return input;
}

int main(int argc, char **argv) {
    randomize::log::log_level = argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL;
    randomize::exception::SignalHandler::activate();

    std::vector<Input> inputs{
            {2, 1, 1, 1, 1, 1, 1, 1, 1, 16},
            {1, 2, 1, 1, 1, 1, 1, 1, 1, 16},
            {1, 1, 2, 1, 1, 1, 1, 1, 1, 16},
            {2, 1, 1, 1, 1, 1, 1, 1, 1, 14},
            {1, 1, 2, 1, 1, 1, 1, 1, 1, 14},
            {1, 2, 1, 1, 1, 1, 1, 1, 1, 14},
            {2, 1, 1, 1, 1, 1, 1, 1, 1, 12},
            {1, 1, 1, 2, 1, 1, 1, 1, 1, 16},
            {1, 1, 1, 1, 2, 1, 1, 1, 1, 18},
            {1, 2, 1, 1, 1, 1, 1, 1, 1, 14},
            {2, 1, 1, 1, 1, 1, 1, 1, 1, 10},
    };

    unsigned long failed = 0;
    failed += TestSuite{"reconstruct", {
            [&]() {
                TemporalDysinet dysinet;

                for (const auto &input : inputs) {
                    dysinet.ingest(input);
                }
                Input testInput{1, 2, 1, 1, 1, 1, 1, 1, 1, 16};
                auto guess = dysinet.reconstruct(testInput);
                guess.pop_back();
                Input expected{1, 2, 1, 1, 1, 1, 1, 1, 1, 16};
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(guess), container_to_string(expected));
            },
    }}.countFailed();

    failed += TestSuite{"predict", {
            [&]() {
                TemporalDysinet dysinet;

                for (const auto &input : inputs) {
                    dysinet.ingest(input);
                }
                Input testInput{1, 2, 1, 1, 1, 1, 1, 1, 1, 16};
                auto reconstructed = dysinet.predict(testInput);
                reconstructed.pop_back();
                Input expected{1, 1, 2, 1, 1, 1, 1, 1, 1, 16};
                ASSERT_STRING_EQUALS_OR_THROW(container_to_string(reconstructed), container_to_string(expected));
            }
    }}.countFailed();

    return failed > 0;
}
