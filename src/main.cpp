#include <iostream>
#include <memory>
#include "commonlibs/log.h"
#include "commonlibs/SignalHandler.h"
#include "network/Dysinet.h"

const std::string HIERARCHICAL_TYPE = "hierarchical";
const std::string ATEMPORAL_TYPE = "atemporal";
const std::string TEMPORAL_TYPE = "temporal";

std::unique_ptr<Dysinet> parseDysinetType(std::string type) {
    if (type == HIERARCHICAL_TYPE) {
        return std::make_unique<HierarchicalDysinet>();
    } else if (type == ATEMPORAL_TYPE) {
        return std::make_unique<AtemporalDysinet>();
    } else if (type == TEMPORAL_TYPE) {
        return std::make_unique<TemporalDysinet>();
    } else {
        throw randomize::exception::StackTracedException{
                "unknown DysinetType. Available types: " + HIERARCHICAL_TYPE + ", " + ATEMPORAL_TYPE + ", " +
                TEMPORAL_TYPE + "."};
    }
}
int main (int argc, char **argv) {
    try {
        randomize::exception::SignalHandler::activate();
        randomize::log::log_level = LOG_INFO_LEVEL;
        auto ptrDysinet = parseDysinetType(argc == 2 ? argv[1] : HIERARCHICAL_TYPE);
        Dysinet &dysinet = ptrDysinet.operator*();
        std::string line;
        std::getline(std::cin, line);
        while (std::cin) {
            if (line == "m" or line == "print_mapping") {
                std::cout << dysinet.mappingToString() << std::endl;
            } else if (line == "s" or line == "print_saturation") {
                std::cout << dysinet.measureSaturation() << std::endl;
            } else {
                auto input = parse(line);
                auto output = dysinet.ingest(input);
                std::cout << to_string(output) << std::endl;
                std::cout << "information: " << dysinet.measureInformation() << std::endl;
            }
            std::getline(std::cin, line);
        }
    } catch (std::exception &e) {
        std::cerr << "program terminated by a thrown exception: " << std::endl << e.what();
        return 1;
    }
    return 0;
}
